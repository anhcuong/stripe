# Stripe Shopping Cart

Live version: http://35.193.97.57/#!/

Main Features:

* Add items into cart
* View cart information
* Checkout using Stripe
* View order and payment history

Technology stack includes python3.7, flask, tinyDB, angularjs and Stripe API.

Folder structure:
```
root/
--backend/
--web/
--README.md
```

## How the Stripe APIs and functionality you are using interact with the user's store?

Building an integration with the Payment Intents API involves two actions: creating and confirming a PaymentIntent. The diagram below demonstrates interactions between user (frontend), backend and Stripe service.

![alt text](StripeDiagram.png "Logo Title Text 1")


## How to prevent user from getting double charged?

There are 3 ways to prevent duplicate charges in Stripe: use Payment Intent flow, disable submit button during payment and perform an idempotent request.

In this demo, I have implmented Payment Intent flow with a provided idempotent key for each of payment intent. The idempotency means for safely retrying requests without accidentally performing the same operation twice, especially when an API call is disrupted in transit.

Stripe default UI's library has also been used to simplify the complexed user's flow. For example: User cannot click the submit button twice at a time.

## Any other potential considerations that an e-commerce store might need to account for that is not included in your app?

Due to the time constraint (I am having final exam for my master class this week), I cannot spend more time on the interface and features. Error has not been handled for all the cases (except payment success and failure) for webhook/ API.

For e-commerce application, we usually need to support different account types (subscription, ...), different payment gateways (bank, paypal, ...) or even user dispute, which require more integration work with Stripe API.

## How to run the app

Clone the source code at: git clone https://anhcuong@bitbucket.org/anhcuong/stripe.git

### Run backend locally
- Use ngrok https to expose your http endpoint and update webhook endpoint in Stripe dashboard
```
ngrok http 5000
```
- Modify secrets (ENDPOINT_SECRET and api_key)
```
export STRIPE_API_KEY=Your API Secret
export STRIPE_ENDPOINT_SECRET=Your Webhook Endpoint Secret
```
- Run server:
```
cd backend/
python server.py
```

API endpoint can be accessed at: http://localhost:5000

### Run frontend locally

- Modify Stripe's client secret in `web/js/client.js`
- Run server
```
cd web/
python -m http.server 8000
```

Web can be accessed at: http://localhost:8000

### Production Setup

Docker and nginx are mainly used for production setup. Live version: http://35.193.97.57/#!/

```
user --- nginx --- static frontend

user --- nginx --- gunicorn --- flask API
```
