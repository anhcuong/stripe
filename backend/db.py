from tinydb import TinyDB, where, Query


orders_db = TinyDB('orders.json')
products_db = TinyDB('products.json')


def _init_db():
    products_db.insert_multiple([
        {"id": 1, "name": "Mask", "image_url": "https://media.karousell.com/media/photos/products/2020/4/30/face_mask_1588222436_4de8b777_progressive_thumbnail.jpg", "price": "7", "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat"},
        {"id": 2,"name": "Surgical Mask", "image_url": "https://media.karousell.com/media/photos/products/2020/4/23/masks_1587659415_797b431d_progressive_thumbnail.jpg", "price": "25", "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat"},
        {"id": 3,"name": "3 Ply Face Mask", "image_url": "https://media.karousell.com/media/photos/products/2020/4/23/3_ply_face_mask_medco_brand_1587630069_16f56cf0_progressive_thumbnail.jpg", "price": "27", "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat"},
        {"id": 4,"name": "iPhone 11 Pro (256GB)", "image_url": "https://media.karousell.com/media/photos/products/2020/5/7/iphone_11_pro_256gb_1588859026_3b8e8710_progressive_thumbnail.jpg", "price": "1800", "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat"},
        {"id": 5,"name": "iPhone 11 (64GB)", "image_url": "https://media.karousell.com/media/photos/products/2020/5/4/iphone_11_64gb_1588566379_20303d1f_progressive_thumbnail.jpg", "price": "1060", "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat"},
        {"id": 6,"name": "Used iPhone X (256GB)", "image_url": "https://media.karousell.com/media/photos/products/2020/4/22/used_iphone_x_256gb_black_9510_1587535703_748cd219_progressive_thumbnail.jpg", "price": "620", "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat"},
    ])


def update_order(order_id, new_info):
    Order = Query()
    orders_db.update(new_info, Order.id == order_id)


# _init_db()
