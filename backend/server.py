import os
import json
import uuid
import time

from flask import Flask, jsonify, request
from flask_cors import CORS
import stripe

from db import orders_db, products_db, where, update_order


app = Flask(__name__)
CORS(app)
stripe.api_key = os.getenv('STRIPE_API_KEY', 'sk_test_UVfqz7mOYz5QSrzcA31cQlA400J3eNtwLp')
stripe.max_network_retries = 5
stripe.api_version = '2020-03-02'
CURRENCY = 'sgd'
ENDPOINT_SECRET = os.getenv('STRIPE_ENDPOINT_SECRET', 'whsec_p9BYsE7UMz1Kz5RSUKLDDUTX9jSrLrGN')
FIXED_DELIVERY_COST = 20


@app.route('/products')
def get_products():
    return jsonify({
        'products': products_db.all()
    }), 200


@app.route('/orders')
def get_orders():
    # Technically we should filter out payment response from stripe
    # But here we want to show the Stripe info in the UI
    return jsonify({
        'orders': orders_db.all()
    }), 200


@app.route('/orders', methods=['POST'])
def create_orders():
    try:
        data = json.loads(request.data)
        random_id = str(uuid.uuid4())
        orders_db.insert({
            "id": random_id,
            "data": data,
        })
        return jsonify({
          'order_id': random_id
        })
    except Exception as e:
        return jsonify(error=str(e)), 403


def calculate_order_amount(order_id):
    order = orders_db.search(where('id') == order_id)[0]
    order_data = order['data']
    total = 0
    for product_info in order_data.values():
        total += int(product_info['quantity']) * float(product_info['item']['price'])
    return 100*(int(total) + FIXED_DELIVERY_COST)


@app.route('/create-payment-intent', methods=['POST'])
def create_payment():
    try:
        data = json.loads(request.data)
        order_id = data['orderId']
        total = calculate_order_amount(order_id)
        intent = stripe.PaymentIntent.create(
            amount=total,
            currency=CURRENCY,
            # We can generate idempotency key and store in db
            # for simplicity i use order_id
            idempotency_key=order_id,
            metadata={
                'order_id': order_id,
                'total': total,
                'user': 'franktran',
            },
        )
        payment_intent = intent['client_secret']
        return jsonify({
            'clientSecret': payment_intent,
            'total': total,
        })
    except Exception as e:
        return jsonify(error=str(e)), 403


@app.route("/webhook", methods=['POST'])
def webhook():
    payload = request.get_data()
    sig_header = request.headers.get('STRIPE_SIGNATURE')
    event = None

    try:
        event = stripe.Webhook.construct_event(
            payload, sig_header, ENDPOINT_SECRET
        )
    except ValueError as e:
        # invalid payload
        return "Invalid payload", 400
    except stripe.error.SignatureVerificationError as e:
        # invalid signature
        return "Invalid signature", 400

    event_dict = event.to_dict()
    if event_dict['type'] == "payment_intent.succeeded":
        intent = event_dict['data']['object']
        order_id = intent['metadata']['order_id']
        update_order(
            order_id,
            {'payment_response': intent, 'status': 'SUCCESS'}
        )

    elif event_dict['type'] == "payment_intent.payment_failed":
        intent = event_dict['data']['object']
        order_id = intent['metadata']['order_id']
        error_message = intent['last_payment_error']['message'] if intent.get('last_payment_error') else None
        update_order(
            order_id,
            {'payment_response': intent, 'status': 'FAILED', 'error_message': error_message}
        )

    return "OK", 200


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
