// Angular
// routing
var app = angular.module("StripeApp", ["ngRoute"]);
var BACKEND_URL="http://localhost:5000/";

app.config(function($routeProvider) {
  $routeProvider
  .when("/", {
    templateUrl : "products.html",
    controller  : 'productController'
  })
  .when("/cart", {
    templateUrl : "cart.html",
    controller  : 'cartController'
  })
  .when("/orders", {
    templateUrl : "orders.html",
    controller  : 'orderController'
  })
});

app.controller('productController', function($scope,  $http, $rootScope, $window) {
  // init
  // $window.localStorage.removeItem("pendingItems")
  // $window.localStorage.removeItem("pendingQuant")
  if (! $rootScope.pendingQuant) {
    $rootScope.pendingQuant = parseInt($window.localStorage.getItem("pendingQuant") || 0);
  }
  if (! $rootScope.pendingItems) {
    $rootScope.pendingItems = JSON.parse($window.localStorage.getItem("pendingItems") || '{}');
  }
  // get all products/
  $http({
    method: 'GET',
    url: BACKEND_URL.concat("products")
  }).then(function (response){
    $scope.products = response.data.products;
  }, function (error) {
    $scope.message = "Internal Server Error";
  });
  // add to cart
  $scope.arr = [];
  $scope.addToCart = function(items, quantity) {
    quantity = parseInt(quantity);
    $rootScope.pendingQuant += quantity;
    console.log(items.id);
    if (! $rootScope.pendingItems[items.id]) {
      $rootScope.pendingItems[items.id] = {"item": items, "quantity": quantity};
    } else {
      $rootScope.pendingItems[items.id][quantity] += quantity;
    }
    $window.localStorage.setItem("pendingQuant", $rootScope.pendingQuant);
    $window.localStorage.setItem("pendingItems", JSON.stringify($rootScope.pendingItems));
  };
});

app.controller('cartController', function($scope,  $http, $rootScope, $window) {
  // init
  if (! $rootScope.pendingQuant) {
    $rootScope.pendingQuant = parseInt($window.localStorage.getItem("pendingQuant") || 0);
  }
  if (! $rootScope.pendingItems) {
    $rootScope.pendingItems = JSON.parse($window.localStorage.getItem("pendingItems") || '{}');
  }
  var total = 0;
  for (var key in $rootScope.pendingItems) {
    item = $rootScope.pendingItems[key]
    total += parseFloat(item.item.price) * parseInt(item.quantity);
  }
  $scope.total = total;
  $scope.openPaymentForm = function() {
    data = $rootScope.pendingItems;
    console.log($rootScope.pendingItems);
    if (data) {
        $http({
          method: 'POST',
          url: BACKEND_URL.concat("orders"),
          data: data,
        }).then(function (response){
          orderId = response.data.order_id;
          // Clean up cart
          $window.localStorage.removeItem("pendingItems")
          $window.localStorage.removeItem("pendingQuant")
          $window.location.href = '/checkout.html?orderId=' + orderId;
        }, function (error) {
          $scope.message = "Internal Server Error";
        });
    }
  }
});

app.controller('orderController', function($scope,  $http, $rootScope, $window) {
  // init
  if (! $rootScope.pendingQuant) {
    $rootScope.pendingQuant = parseInt($window.localStorage.getItem("pendingQuant") || 0);
  }
  if (! $rootScope.pendingItems) {
    $rootScope.pendingItems = JSON.parse($window.localStorage.getItem("pendingItems") || '{}');
  }

  // get all orders/
  $http({
    method: 'GET',
    url: BACKEND_URL.concat("orders")
  }).then(function (response){
    $scope.orders = response.data.orders;
  }, function (error) {
    $scope.message = "Internal Server Error";
  });

});

// app.controller('checkoutController', function($scope,  $http, $rootScope, $window) {
//   // init
//   if (! $rootScope.pendingQuant) {
//     $rootScope.pendingQuant = parseInt($window.localStorage.getItem("pendingQuant") || 0);
//   }
//   if (! $rootScope.pendingItems) {
//     $rootScope.pendingItems = JSON.parse($window.localStorage.getItem("pendingItems") || '{}');
//   }
// });
